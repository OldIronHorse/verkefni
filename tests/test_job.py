from unittest.mock import MagicMock, call
from verkefni.task import Task
from verkefni.job import SumOfSquares, WordCount


class TestSumOfSquares:
    def test_start(self):
        send_task = MagicMock()
        sos = SumOfSquares(send_task, [0, 2, 5, 4])

        sos.start()

        send_task.assert_has_calls([
            call(Task(id='sos-0-0',
                      function='arithmetic.square',
                      input_data=0)),
            call(Task(id='sos-0-1',
                      function='arithmetic.square',
                      input_data=2)),
            call(Task(id='sos-0-2',
                      function='arithmetic.square',
                      input_data=5)),
            call(Task(id='sos-0-3',
                      function='arithmetic.square',
                      input_data=4))])

    def test_on_result_valid_first_square_result(self):
        send_task = MagicMock()
        sos = SumOfSquares(send_task, [0, 2, 5, 4])
        sos.start()
        send_task.reset_mock()

        sos.on_result(dict(id='sos-0-1', output_data=4))

        send_task.assert_not_called()
        assert sos.squares == {1: 4}
        assert sos.result is None

    def test_on_result_valid_squares_complete(self):
        send_task = MagicMock()
        sos = SumOfSquares(send_task, [2, 5])
        sos.start()
        send_task.reset_mock()

        sos.on_result(dict(id='sos-0-1', output_data=25))
        sos.on_result(dict(id='sos-0-0', output_data=4))

        send_task.assert_called_once_with(Task(id='sos-1-0',
                                               function='arithmetic.sum',
                                               input_data=[4, 25]))

    def test_on_result_valid_sum_complete(self):
        send_task = MagicMock()
        sos = SumOfSquares(send_task, [2, 5])
        sos.start()
        send_task.reset_mock()
        sos.on_result(dict(id='sos-1-0', output_data=29))

        send_task.assert_not_called()
        assert sos.is_complete()
        assert sos.result == 29


class TestWordCount:
    def test_start(self):
        send_task = MagicMock()
        wc = WordCount(send_task,
                       'Some lines\n'
                       'of text that\n'
                       'mean nothing')
        wc.start()

        send_task.assert_has_calls([
            call(Task(id='wc-0',
                      function='lexical.wordcount',
                      input_data='Some lines')),
            call(Task(id='wc-1',
                      function='lexical.wordcount',
                      input_data='of text that')),
            call(Task(id='wc-2',
                      function='lexical.wordcount',
                      input_data='mean nothing')),
            call(Task(id='s-0',
                      function='lexical.sort',
                      input_data='Some lines')),
            call(Task(id='s-1',
                      function='lexical.sort',
                      input_data='of text that')),
            call(Task(id='s-2',
                      function='lexical.sort',
                      input_data='mean nothing'))],
            any_order=True)

        assert not wc.is_complete()

    def test_on_result_valid_wordcount(self):
        send_task = MagicMock()
        wc = WordCount(send_task,
                       'Some lines\n'
                       'of text that\n'
                       'mean nothing')
        wc.start()
        send_task.reset_mock()

        wc.on_result(dict(id='wc-1', output_data=3))

        send_task.assert_not_called()
        assert wc.wc_by_line == {1: 3}
        assert wc.sorted_lines == dict()

    def test_on_result_valid_sort(self):
        send_task = MagicMock()
        wc = WordCount(send_task,
                       'Some lines\n'
                       'of text that\n'
                       'mean nothing')
        wc.start()
        send_task.reset_mock()

        wc.on_result(dict(id='s-0', output_data=['lines', 'Some']))

        send_task.assert_not_called()
        assert wc.wc_by_line == dict()
        assert wc.sorted_lines == {0: ['lines', 'Some']}

    def test_on_result_to_completion(self):
        send_task = MagicMock()
        wc = WordCount(send_task,
                       'Some lines\n'
                       'of text that\n'
                       'mean nothing')
        wc.start()
        send_task.reset_mock()

        wc.on_result(dict(id='s-0', output_data=['lines', 'Some']))
        wc.on_result(dict(id='s-2', output_data=['mean', 'nothing']))
        wc.on_result(dict(id='s-1', output_data=['of', 'text', 'that']))
        wc.on_result(dict(id='wc-1', output_data=3))
        wc.on_result(dict(id='wc-2', output_data=2))
        wc.on_result(dict(id='wc-0', output_data=2))

        assert wc.is_complete()
