import os.path
from verkefni.common import get_logger
from verkefni.task import Task

logger = get_logger('job')


class FileIngest:
    def __init__(self, send_task, file):
        self.send_task = send_task
        self.file = file
        self.lo_res_dir = os.path.join(os.path.dirname(file), 'lo-res')
        self.target_languages = ['en', 'cn', 'es']
        self.metadata = None
        self.lo_res_file = None
        self.transcription = None
        self.translations = dict()
        self.error = None

    def is_complete(self):
        return (self.error is not None
                or (all([r is not None for r
                         in [self.metadata, self.lo_res_file,
                             self.transcription]])
                    and len(self.translations) == len(self.target_languages)))

    def start(self):
        self.send_task(Task(id='extract-metadata',
                            function='fileinspect.metadata',
                            input_data=self.file))

    def on_result(self, result):
        try:
            match result['id']:
                case 'extract-metadata':
                    self.metadata = result['output_data']
                    logger.info('Got metadata. Validating...')
                    self.metadata['Media Duration']
                    # TODO: check format etc. is supported
                    self.send_task(Task(id='transcode-to-lo-res',
                                        function='video.transcode',
                                        input_data={
                                            'file': self.file,
                                            'resolution': '180x144',
                                            'lo-res-dir': self.lo_res_dir,
                                            'metadata': self.metadata}))
                case 'transcode-to-lo-res':
                    self.lo_res_file = result['output_data']['lo-res-file']
                    self.send_task(Task(id='analyse-scenes',
                                        function='analysis.scenes',
                                        input_data=dict(
                                            file=self.lo_res_file)))
                    self.send_task(Task(id='transcribe-audio',
                                        function='analysis.transcribe',
                                        input_data=dict(
                                            file=self.lo_res_file)))
                case 'transcribe-audio':
                    # TODO: file or test here?
                    self.transcription = result['output_data']
                    for language in self.target_languages:
                        self.send_task(Task(id=f'translate-{language}',
                                            function='analysis.translate',
                                            input_data={
                                                'text': self.transcription,
                                                'target-language': language,
                                                }))
                case id:
                    match id.split('-', 1):
                        case ['translate', language]:
                            self.translations[language] = (
                                result['output_data']['translation'])
        except KeyError as e:
            self.error = (result['id'], e)


class SumOfSquares:
    def __init__(self, send_task, input_values):
        self.send_task = send_task
        self.input_values = input_values
        self.squares = dict()
        self.result = None

    def start(self):
        for i, v in enumerate(self.input_values):
            task_id = f'sos-0-{i}'
            self.send_task(Task(id=task_id, function='arithmetic.square',
                                input_data=v))

    def on_result(self, result):
        match result['id'].split('-'):
            case ['sos', '0', i]:
                self.squares[int(i)] = result['output_data']
                if len(self.squares) == len(self.input_values):
                    self.send_task(
                            Task(id='sos-1-0',
                                 function='arithmetic.sum',
                                 input_data=[self.squares[n] for n
                                             in range(len(self.squares))]))
            case ['sos', '1', '0']:
                self.result = result['output_data']

    def is_complete(self):
        return self.result is not None


class WordCount:
    def __init__(self, send_task, text):
        self.send_task = send_task
        self.text = text
        self.wc_by_line = dict()
        self.sorted_lines = dict()
        self.pending_tasks = set()

    def is_complete(self):
        return not self.pending_tasks

    def start(self):
        for i, line in enumerate(self.text.split('\n')):
            task_id = f'wc-{i}'
            self.send_task(Task(id=task_id, function='lexical.wordcount',
                                input_data=line))
            self.pending_tasks.add(task_id)
            task_id = f's-{i}'
            self.send_task(Task(id=task_id, function='lexical.sort',
                                input_data=line))
            self.pending_tasks.add(task_id)

    def on_result(self, result):
        if result['id'] in self.pending_tasks:
            match result['id'].split('-'):
                case ['wc', i]:
                    self.wc_by_line[int(i)] = result['output_data']
                case ['s', i]:
                    self.sorted_lines[int(i)] = result['output_data']
            self.pending_tasks.remove(result['id'])
