FROM python:3.10-alpine

RUN adduser -D verkefni
WORKDIR /home/verkefni
COPY boot.sh ./
RUN chmod +x boot.sh
USER verkefni

COPY verkefni verkefni
COPY bin bin
COPY setup.py README.md ./
RUN python -m venv venv
RUN venv/bin/pip install .

ENTRYPOINT ["./boot.sh"]
